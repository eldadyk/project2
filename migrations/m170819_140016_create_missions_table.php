<?php

use yii\db\Migration;

/**
 * Handles the creation of table `missions`.
 * Has foreign keys to the tables:
 *
 * - `project`
 */
class m170819_140016_create_missions_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('missions', [
            'id' => $this->primaryKey(),
            'ipd' => $this->integer()->notNull(),
            'title' => $this->string(),
            'bodyMission' => $this->string(),
            'status' => $this->string(),
            'endDateRequired' => $this->integer(),
            'actualEnddate' => $this->integer(),
        ]);

        // creates index for column `ipd`
        $this->createIndex(
            'idx-missions-ipd',
            'missions',
            'ipd'
        );

        // add foreign key for table `project`
        $this->addForeignKey(
            'fk-missions-ipd',
            'missions',
            'ipd',
            'project',
            'ipd',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `project`
        $this->dropForeignKey(
            'fk-missions-ipd',
            'missions'
        );

        // drops index for column `ipd`
        $this->dropIndex(
            'idx-missions-ipd',
            'missions'
        );

        $this->dropTable('missions');
    }
}
