<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 * Has foreign keys to the tables:
 *
 * - `project`
 */
class m170819_140753_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'ipd' => $this->integer()->notNull(),
            'name' => $this->string(),
            'username' => $this->string(),
            'role' => $this->string(),
        ]);

        // creates index for column `ipd`
        $this->createIndex(
            'idx-users-ipd',
            'users',
            'ipd'
        );

        // add foreign key for table `project`
        $this->addForeignKey(
            'fk-users-ipd',
            'users',
            'ipd',
            'project',
            'ipd',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `project`
        $this->dropForeignKey(
            'fk-users-ipd',
            'users'
        );

        // drops index for column `ipd`
        $this->dropIndex(
            'idx-users-ipd',
            'users'
        );

        $this->dropTable('users');
    }
}
