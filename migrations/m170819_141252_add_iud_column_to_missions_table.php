<?php

use yii\db\Migration;

/**
 * Handles adding iud to table `missions`.
 * Has foreign keys to the tables:
 *
 * - `users`
 */
class m170819_141252_add_iud_column_to_missions_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('missions', 'iud', $this->integer()->notNull());

        // creates index for column `iud`
        $this->createIndex(
            'idx-missions-iud',
            'missions',
            'iud'
        );

        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-missions-iud',
            'missions',
            'iud',
            'users',
            'iud',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `users`
        $this->dropForeignKey(
            'fk-missions-iud',
            'missions'
        );

        // drops index for column `iud`
        $this->dropIndex(
            'idx-missions-iud',
            'missions'
        );

        $this->dropColumn('missions', 'iud');
    }
}
