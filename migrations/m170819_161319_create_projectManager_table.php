<?php

use yii\db\Migration;

/**
 * Handles the creation of table `projectManager`.
 * Has foreign keys to the tables:
 *
 * - `users`
 */
class m170819_161319_create_projectManager_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('projectManager', [
            'id' => $this->primaryKey(),
            'iud' => $this->integer()->notNull(),
        ]);

        // creates index for column `iud`
        $this->createIndex(
            'idx-projectManager-iud',
            'projectManager',
            'iud'
        );

        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-projectManager-iud',
            'projectManager',
            'iud',
            'users',
            'iud',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `users`
        $this->dropForeignKey(
            'fk-projectManager-iud',
            'projectManager'
        );

        // drops index for column `iud`
        $this->dropIndex(
            'idx-projectManager-iud',
            'projectManager'
        );

        $this->dropTable('projectManager');
    }
}
