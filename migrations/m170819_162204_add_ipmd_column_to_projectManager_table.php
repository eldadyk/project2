<?php

use yii\db\Migration;

/**
 * Handles adding ipmd to table `projectManager`.
 */
class m170819_162204_add_ipmd_column_to_projectManager_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('projectManager', 'ipmd', $this->integer()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('projectManager', 'ipmd');
    }
}
