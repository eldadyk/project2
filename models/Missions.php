<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "missions".
 *
 * @property integer $imd
 * @property integer $ipd
 * @property string $title
 * @property string $bodyMission
 * @property string $status
 * @property integer $endDateRequired
 * @property integer $actualEnddate
 * @property integer $iud
 */
class Missions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'missions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ipd', 'iud'], 'required'],
            [['ipd', 'endDateRequired', 'actualEnddate', 'iud'], 'integer'],
            [['title', 'bodyMission', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'imd' => 'Imd',
            'ipd' => 'Ipd',
            'title' => 'Title',
            'bodyMission' => 'Body Mission',
            'status' => 'Status',
            'endDateRequired' => 'End Date Required',
            'actualEnddate' => 'Actual Enddate',
            'iud' => 'Iud',
        ];
    }
}
