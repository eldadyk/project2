<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projectManager".
 *
 * @property integer $iud
 */
class ProjectManager extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projectManager';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iud'], 'required'],
            [['iud'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iud' => 'Iud',
        ];
    }
}
