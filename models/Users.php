<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $iud
 * @property integer $ipd
 * @property string $name
 * @property string $username
 * @property string $role
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ipd'], 'required'],
            [['ipd'], 'integer'],
            [['name', 'username', 'role'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iud' => 'Iud',
            'ipd' => 'Ipd',
            'name' => 'Name',
            'username' => 'Username',
            'role' => 'Role',
        ];
    }
}
