<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Missions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="missions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ipd')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bodyMission')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'endDateRequired')->textInput() ?>

    <?= $form->field($model, 'actualEnddate')->textInput() ?>

    <?= $form->field($model, 'iud')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
