<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MissionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="missions-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'imd') ?>

    <?= $form->field($model, 'ipd') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'bodyMission') ?>

    <?= $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'endDateRequired') ?>

    <?php // echo $form->field($model, 'actualEnddate') ?>

    <?php // echo $form->field($model, 'iud') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
